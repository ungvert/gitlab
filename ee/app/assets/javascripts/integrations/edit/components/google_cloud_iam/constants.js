export const STATE_EMPTY = 'empty';
export const STATE_FORM = 'form';
export const STATE_GUIDED = 'guided';
export const STATE_INITIAL = 'initial';
export const STATE_MANUAL = 'manual';
